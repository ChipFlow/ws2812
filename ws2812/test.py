from amaranth_boards.icebreaker import ICEBreakerPlatform

class Test():
    """
    Return a list of random ingredients as strings.

    >>> from amaranth import *
    >>> from amaranth.build import *
    >>> from amaranth_boards.icebreaker import ICEBreakerPlatform
    >>> from src.ws2812 import Top
    >>> ICEBreakerPlatform().build(Top())
    """

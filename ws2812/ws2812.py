from amaranth import *
from amaranth.build import *
from amaranth_boards.icebreaker import ICEBreakerPlatform


class Top(Elaboratable):
    """
    WS2812 led driver

    :param kind: leds : number of leds
    :type kind: list[str] or None
    :return:  the object

    .. mermaid::

       sequenceDiagram
          participant Alice
          participant Bob
          Alice->John: Hello John, how are you?
          loop Healthcheck
              John->John: Fight against hypochondria
          end
          Note right of John: Rational thoughts <br/>prevail...
          John-->Alice: Great!
          John->Bob: How about you?
          Bob-->John: Jolly good!

    .. wavedrom:: pwm.json

    """
    def elaborate(self, platform):
        m = Module()

        platform.add_resources([
            Resource("ws2812", 0, Pins("42", dir="o"), Attrs(IO_STANDARD="SB_LVCMOS")),
        ])
        ws2812 = platform.request("ws2812")

        delay = Signal(range(2 + 1))
        total = Signal(range(262 + 1))
        count = Signal(range(4 + 1))
        pixel = Signal(range(24 + 1))

        m.d.sync += delay.eq(delay + 1)
        with m.If(delay == 2):
            m.d.sync += delay.eq(0)

            m.d.sync += count.eq(count + 1)
            with m.If(count == 0):
                m.d.sync += ws2812.eq(1)
            with m.If(count == 1):
                m.d.sync += ws2812.eq((pixel >= 8) & (pixel <= 16))
            with m.If(count == 4):
                m.d.sync += ws2812.eq(0)
            with m.If(count == 5):
                m.d.sync += count.eq(0)

                with m.If(pixel == 23):
                    m.d.sync += pixel.eq(0)
                    with m.If(total != 262):
                        m.d.sync += total.eq(total + 1)
                with m.Else():
                    m.d.sync += pixel.eq(pixel + 1)

            with m.If((total < 10) | (total == 262)):
                m.d.sync += ws2812.eq(0)

        return m



#ICEBreakerPlatform().build(Top(), do_program=True)
"""
from amaranth.sim import Simulator

dut = Top()
def bench():
    # Disabled counter should not overflow.
    for _ in range(300):
        yield

sim = Simulator(dut)
sim.add_clock(10e-6) # 1 MHz
sim.add_sync_process(bench)
with sim.write_vcd("ws2812.vcd"):
    sim.run()
"""
